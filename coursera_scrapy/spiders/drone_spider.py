from datetime import datetime
import scrapy


class CourseraSpider(scrapy.Spider):
    runtime = datetime.now().strftime("%Y-%m-%d_%H%M")
    name = "coursera_spider"
    allowed_domains = ["coursera.org"]
    start_urls = ["https://www.coursera.org/degrees/data-science"]

    def parse(self, response):
        courses = response.css("div.css-1foy7y2")
        for number, course in enumerate(courses):

            duedate = course.css("p.cds-119.css-1gxnzij.cds-121::text").get()
            if duedate is None:
                duedate = "No Due Date"

            institution = course.css(
                "p.cds-119.css-pa6u6k.cds-121::text"
            ).get()
            if institution is None:
                institution = "No Due Date"

            yield dict(
                    number=number,
                    runtime=self.runtime,
                    name=course.css("h3::text").get(),
                    institution=institution,
                    duedate=duedate,
            )

        # next_page = response.css([next page])
        # if next_page is not None:
        #   yield response.follow(next_page, callback=self.parse)
